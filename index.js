/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 10
*/
const port=5000;
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

let PhoneBook = require('./PhoneBook').PhoneBook;
let DB = new PhoneBook();

app.listen(port,()=>{
  console.log(`Server started on ${port} port...`);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

//RPC
function getRPCError(text,id=0){
  return {'jsonrpc':'2.0', 'error':{'code':'-1', 'message':text}, 'id': id}
}
function getRPCAnswer(data,id){
  return {'jsonrpc':'2.0', 'result':data, 'id': id}
}
function formatRPCResponse(err,res,id=0){
  return (err||!res)?getRPCError('Internal error',id):getRPCAnswer(res,id);
}

app.use('/rpc', function(req,res,next){
  let message='';
  if (req.body.jsonrpc!='2.0'){
    message+='Only jsonrpc 2.0 is supported. ';
  }
  if (!['list','add','update','delete','search','show'].includes(req.body.method)){
    message+='Method is not supported. ';
  }
  if (!req.body.id){
    message+='Request ID not found. ';
  }
  if (['update', 'delete','show'].includes(req.body.method)
    &&(!req.body.params||!req.body.params.id)){
        message+='Record not found. ';
  }
  if (['add','update'].includes(req.body.method)&&(!req.body.params||!req.body.params.firstName||!req.body.params.lastName||!req.body.params.phone)){
        message+='firstName, lastName and phone are required for this operation. ';
  }
  if (message!=''){
    return res.status(400).json(getRPCError(message,req.body.id?req.body.id:0));
  }
  next();
});


app.post('/rpc',(req,res)=>{
  switch (req.body.method) {
    case 'list':
    DB.getAll((err,result)=>{
      res.json(formatRPCResponse(err,result,req.body.id));
    });
    break;
    case 'show':
    DB.getRecord(req.body.params.id,(err,result)=>{
    res.json(formatRPCResponse(err,result,req.body.id));
    });

    break;
    case 'add':
    DB.addRecord(req.body.params.firstName, req.body.params.lastName, req.body.params.phone,(err,result)=>{
      res.json(formatRPCResponse(err,"Done",req.body.id));
    });
    break;
    case 'update':
    DB.updateRecord(req.body.params.id, req.body.params.firstName, req.body.params.lastName, req.body.params.phone,(err,result)=>{
      res.json(formatRPCResponse(err,"Done",req.body.id));
    });
    break;
    case 'delete':
    DB.deleteRecord(req.body.params.id,(err,result)=>{
      res.json(formatRPCResponse(err,"Done",req.body.id));
    });
    break;
    case 'search':
    DB.searchRecord(req.body.params.firstName, req.body.params.lastName, req.body.params.phone,(err,result)=>{
      res.json(formatRPCResponse(err,result,req.body.id));
    });
    break;
  }
});

app.all('*', (req,res)=>{
  res.status(404).json({'error':'method not found. Use POST to /rpc with one of methods:[list, add(firstName,lastName,phone), update(id,firstName,lastName,phone), delete(id), search(firstName and/or lastName and/or phone)]'});
});
