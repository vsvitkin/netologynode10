#Приложение работает с базой данных mongoDB, предоставляет API RPC
Обращенийе в соответствии со спецификацией RPC 2.0:
 Адрес: 127.0.0.1:5000/rpc
 Метод: post
 Примеры запросов:

##Получение полного списка записей из базы:
`{
	"jsonrpc":"2.0",
	"method":"list",
	"id":"123"
}`

##Добавление записи в базу
`{
	"jsonrpc":"2.0",
	"method":"add",
	"params":
	{
		"firstName":"Vasily",
		"lastName":"Svitkin",
		"phone":"123456"
	},
	"id":"123"
}`

##Удаление записи из базы по id
`{
	"jsonrpc":"2.0",
	"method":"delete",
	"params":
	{
		"id":"590c48c37e98652f8c3fad68"
	},
	"id":"123"
}`


##Вывод одной записи из базы по id
`{
	"jsonrpc":"2.0",
	"method":"show",
	"params":
	{
		"id":"590c48d723fecd41a4ab2242"
	},
	"id":"123"
}`

##Поиск записей в базе (логика поиска OR - то есть выводятся записи, у которых совпадает хотя бы одно из полей (firstName, lastName или phone). В примере поиск только по firstName)
`{
	"jsonrpc":"2.0",
	"method":"search",
	"params":
	{
		"firstName":"Vasily"
	},
	"id":"123"
}`

##Обновление записи (по id)
`{
	"jsonrpc":"2.0",
	"method":"update",
	"params":
	{
		"id":"590c48d723fecd41a4ab2242",
		"firstName":"Василий",
		"lastName":"Свиткин",
		"phone":"123456"
	},
	"id":"123"
}`
