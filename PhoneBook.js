class PhoneBook {
  constructor(){
    this.mongo = require('mongodb');
    this.client = this.mongo.MongoClient;
    this.connected=false;
    const url = 'mongodb://localhost:27017/vsvitkin';
    const collectioName='phonebook';

    console.log(`Connecting to ${url}...`)
    this.client.connect(url, (err, db)=>{
      if (err) {
        console.log(`Can not connect to '${url}'. Error text: ${err}`);
      } else {
        console.log(`Connected to '${url}'.`);
        this.connected=true;
        this.db=db;
        this.collection = db.collection(collectioName);
      }
    });
  }
  isConnected(){
    if (!this.connected) console.log('Error: not connected');
      return this.connected;
  }
  addRecord(firstName, lastName, phone, cb){
    if (!this.isConnected()) return false;
    this.collection.insert({'firstName':firstName, 'lastName':lastName, 'phone':phone},(err,res)=>cb(err,res));
  }
  getRecord(id,cb){
    if (!this.isConnected()) return false;
    this.collection.find({'_id':new this.mongo.ObjectID(id)}).toArray((err,res)=>cb(err,res));
  }
  getAll(cb){
    if (!this.isConnected()) return false;
    this.collection.find().toArray((err,res)=>cb(err,res));
  }
  deleteRecord(id,cb){
    if (!this.isConnected()) return false;
    this.collection.remove({'_id':new this.mongo.ObjectID(id)},(err,res)=>cb(err,res));
  }
  updateRecord(id,firstName, lastName, phone,cb){
    if (!this.isConnected()) return false;
    this.collection.update({'_id':new this.mongo.ObjectID(id)},{'firstName':firstName, 'lastName':lastName, 'phone':phone},(err,res)=>cb(err,res));
  }
  searchRecord(firstName, lastName, phone,cb){
    if (!this.isConnected()) return false;
    this.collection.find({$or:[{'firstName':firstName}, {'lastName':lastName}, {'phone':phone}]}).toArray((err,res)=>cb(err,res));
  }
}

module.exports = {
  PhoneBook
};
